/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * Tachyon metadata utility functions
 *
**/

'use strict'

var uuid = require('uuid')
    ;

const validLocale = ['az','cz','de','de_AT','de_CH','en','en_AU','en_BORK','en_CA','en_GB','en_IE','en_IND','en_US','en_au_ocker','es','es_MX','fa','fr','fr_CA','ge','id_ID','it','ja','ko','nb_NO','nep','nl','pl','pt_BR','ru','sk','sv','tr','uk','vi','zh_CN','zh_TW'];
const validSC = ['public','internal','confidential'];
const validIngestionType = ['single','batch'];

/**
 * @function buildMetadata
 * Function to create standard Tachyon Event Metadata
 * @param {string} pipeline - (Required) Pipeline name
 * @param {string} source - (Required) Event source
 * @param {string} eventName - (Required) The event name
 * @param {integer} schemaVersion - (Optional) The schema version for validation
 * @param {string} externalId - (Optional) ID associated with the event
 * @param {string} brand - (Optional) Brand associated with the event
 * @param {string} locale - (Optional) The localisation of the data
 * @param {string} country - (Optional) Country of origin
 * @param {string} service - (Optional) The name of source data application/service
 * @param {string} module - (Optional) The name of a module within an application relating to the source of event data
 * @param {boolean} personalData - (Optional) Whether the event data contains personal data 
 * @param {string} securityClass - (Optional) The security classification of the event data (public/internal/confidential)
 * @param {string} ingestionType - (Optional) The data ingestion type (single/batch)
 * @param {boolean} discoverable - (Optional) Whether the event should be onwardly searchable/discoverable
 * @param {array} tags - (Optional) name/value tags
 * @param {boolean} synthetic - (Optional) Whether the event is synthetic (test) data
 * @return {object} - The metadata object
**/
exports.buildMetadata = function buildMetadata(pipeline, source, eventName, schemaVersion, externalId, brand, locale, country, service, module, personalData, securityClass, ingestionType, discoverable, tags, synthetic) {
    if (!pipeline || !source || !eventName) {
        throw new Error('Required metadata fields missing');
    }
    if (!schemaVersion || isNaN(parseInt(schemaVersion)) || schemaVersion < 1) {
        schemaVersion = 1;
    }
    if (!externalId || externalId.length === 0) {
        externalId = uuid.v4();
    }
    if (!locale || locale.length === 0) {
        locale = 'en_GB';
    }
    else if (validLocale.indexOf(locale) < 0) {
        throw new Error('Locale must be one of ' + validLocale.toString());
    }
    if (!securityClass || securityClass.length === 0) {
        securityClass = 'public';
    }
    else if (validSC.indexOf(securityClass) < 0) {
        throw new Error('Security Classification must be one of ' + validSC.toString());
    }
    if (!ingestionType || ingestionType.length === 0) {
        ingestionType = 'single';
    }
    else if (validIngestionType.indexOf(ingestionType) < 0) {
        throw new Error('Ingestion Type must be one of ' + validIngestionType.toString());
    }
    if (!tags || !Array.isArray(tags)) {
        tags = [];
    }
    if (personalData === undefined) {
        personalData = false;
    }
    if (discoverable === undefined) {
        discoverable = true;
    }
    if (synthetic === undefined) {
        synthetic = false;
    }

    var metadata = {
        locale: locale,
        pipeline: pipeline,
        source: source,
        eventName: eventName,
        version: schemaVersion,
        externalId: externalId,
        brand: brand || '',
        country: country || '',
        service: service || '',
        module: module || '',
        occurredAt: new Date().toISOString(),
        submittedAt: new Date().toISOString(),
        correlationId: '',
        tags: tags,
        secondarySchemas: [
        ],
        identity: {
        },
        pd: personalData,
        sc: securityClass,
        ingestionType: ingestionType,
        discoverable: discoverable,
        synthetic: synthetic
    };
    
    return metadata;
}

/**
 * @function buildClickMetadata
 * Function to create Clickstream metadata
 * @param {string} eventName - (Required) The event name
 * @param {string} correlationId - (Optional) The id of a corresponding event
 * @param {string} brand - (Optional) Brand associated with the event
 * @param {string} service - (Optional) The name of source data application/service
 * @param {string} module - (Optional) The name of a module within an application relating to the source of event data
 * @param {string} country - (Optional) Country of origin
 * @param {string} locale - (Optional) The localisation of the data
 * @param {boolean} synthetic - (Optional) Whether the event is synthetic (test) data
 * @return {object} - The metadata object
**/
exports.buildClickMetadata = function buildClickMetadata(eventName, correlationId, brand, service, module, country, locale, synthetic) {
    if (!eventName) {
        throw new Error('eventName must be supplied');
    }
    if (!locale || locale.length === 0) {
        locale = 'en_GB';
    }
    else if (validLocale.indexOf(locale) < 0) {
        throw new Error('Locale must be one of ' + validLocale.toString());
    }
    if (synthetic === undefined) {
        synthetic = false;
    }

    var metadata = {
        locale: locale,
        pipeline: 'Website',
        source: 'Vivid',
        eventName: eventName,
        version: 1,
        externalId: uuid.v4(),
        brand: brand || '',
        country: country || '',
        service: service || '',
        module: module || '',
        occurredAt: new Date().toISOString(),
        submittedAt: new Date().toISOString(),
        correlationId: correlationId || '',
        pd: false,
        sc: 'public',
        ingestionType: 'single',
        discoverable: true,
        synthetic: synthetic,
        tachyon: {
            receivedAt: new Date().toISOString()
        }
    };
    
    return metadata;
}