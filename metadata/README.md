# Metadata Utilities

Helper functions for creating Tachyon metadata objects.

## Function `buildMetadata`

Create a standard Tachyon metadata object

```js
var metadata = tachyonUtils.metadata.buildMetadata(pipeline, source, eventName, schemaVersion, externalId, brand, locale, country, service, module, personalData, securityClass, ingestionType, discoverable, tags, synthetic);
```

Parameter | Type | Required | Valid Options | Information
--- | --- | --- | --- | ---
pipeline | string | Yes | FileUpload, Braze, SAP, CRM, Website | Pipeline name
source | string | Yes | Unknown, API Pull, Vivid,<br>Application Push, File Upload | Event source
eventName | string | Yes | Any | The event name
schemaVersion | integer | No | >= 1 | The schema version for validation
externalId | string | No | Any | ID associated with the event
brand | string | No | Any | Brand associated with the event
locale | string | No | az, cz, de, de_AT, de_CH, en, en_AU,<br>en_BORK, en_CA, en_GB, en_IE, en_IND,<br>en_US, en_au_ocker, es, es_MX, fa, fr, fr_CA,<br>ge, id_ID, it, ja, ko, nb_NO, nep, nl, pl, pt_BR,<br>ru, sk, sv, tr, uk, vi, zh_CN, zh_TW | The localisation of the data
country | string | No | Any | Country of origin
service | string | No | Any | The name of source data application/service
module | string | No | Any | The name of a module within an application relating to the source of event data
personalData | boolean | No | true, false | Whether the event data contains personal data 
securityClass | string | No | public, internal, confidential | The security classification of the event data (public/internal/confidential)
ingestionType | string | No | single, batch | The data ingestion type
discoverable | boolean | No | true, false | Whether the event should be onwardly searchable/discoverable
tags | array | No | name/value object array | Information tags
synthetic | boolean | No | true, false | Whether the event is synthetic (test) data

---

## Function `buildClickMetadata`

Create a Clickstream metadata object

```js
var metadata = tachyonUtils.metadata.buildClickMetadata(eventName, correlationId, brand, service, module, country, locale, synthetic);
```

Parameter | Type | Required | Valid Options | Information
--- | --- | --- | --- | ---
eventName | string | Yes | Any | The event name
correlationId | string | No | Any | Correlating event ID
brand | string | No | Any | Brand associated with the event
service | string | No | Any | The name of source data application/service
module | string | No | Any | The name of a module within an application relating to the source of event data
country | string | No | Any | Country of origin
locale | string | No | az, cz, de, de_AT, de_CH, en, en_AU,<br>en_BORK, en_CA, en_GB, en_IE, en_IND,<br>en_US, en_au_ocker, es, es_MX, fa, fr, fr_CA,<br>ge, id_ID, it, ja, ko, nb_NO, nep, nl, pl, pt_BR,<br>ru, sk, sv, tr, uk, vi, zh_CN, zh_TW | The localisation of the data
synthetic | boolean | No | true, false | Whether the event is synthetic (test) data

---

[Return to root documentation](./../README.md#tachyon-utilities)