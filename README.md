# Tachyon Utilities

Tachyon Utilities provides a number of helper functions which are common to multiple applications.
___

## Requirements

The helper functions are written in Node.js and can be referenced within other applications via the package dependencies:

```json
    "dependencies": {
        "tac-1d-utils": "git+ssh://git@bitbucket.org/1digitall/tac-1d-utils"
    }
```

It is recommended that the dependecy is specified using the version tag `git+ssh://git@bitbucket.org/1digitall/tac-1d-utils#<version>`. For example:

```json
    "dependencies": {
        "tac-1d-utils": "git+ssh://git@bitbucket.org/1digitall/tac-1d-utils#v0.1.0"
    }
```

The repository is not public and as such the use of the package is limited to 1Digit users and appropriate access keys.

## Helper Functions

The helper functions available with the Tachyon Utilities project are grouped by functionality. Details of the functions and how they are called is described below.

### Metadata Utilities

Helper functions for creating Tachyon metadata objects. See [Metadata Utilities](./metadata/README.md#metadata-utilities)

### Telemetry Module

Module providing common telemetry capabilities for Tachyon applications. See [Telemetry Module](./telemetry/README.md#tachyon-telemetry)

### Authentication Module

Module providing JWT verification and authentication capabilities. See [Authentication Module](./authentication/README.md#tachyon-authentication)

### Messaging Module

Module providing common system message delivery for Tachyon applications. See [Messaging Module](./messaging/README.md#tachyon-messaging)

---