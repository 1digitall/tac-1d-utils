/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * Authentication constants
 *
*/

'use strict'

module.exports = Object.freeze({
    DEFAULT_AWS_REGION: 'eu-west-1',
    DEFAULT_AWS_RETRIES: 5
});