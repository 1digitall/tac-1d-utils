# Tachyon Authentication

The Tachyon Authentication package is a common utility for any server application that requires endpoint OAuth 2.0 authentication.

Authentication is carried out by validating the Bearer Token supplied in HTTP requests. The token is checked for integrity via the encryption public key, that it hasn't expired, and that the encoded system access rights meet the requirements of the endpoint in question. Clients may utilise the Tachyon signin service in order to create tokens for use in requests.

The authentication module provides two options for middleware express components - one for standard web applications and another for socket.io versions.

---

## Prepare Authentication

The Tachyon Authentication package needs to be enabled and started correctly before it can be used. This is carried out via a preparation function in order to obtain the public keys held in AWS Secrets Manager, as well as set the API code for the application in question. 

```js
tacUtils.authentication.prepareAuthentication(api, tokens, enabled)
```

Parameter | Type | Required | Information
--- | --- | --- | --- | ---
api | string | Yes | The 2 character code for the application. Users must have this code in their system access.
tokens | string | Yes | The name of the AWS Secret holding the token public keys
enabled | boolean | Yes | Whether to enable authentication across endpoints. If set to false, no auth is applied.

Only once the preparation has completed should the routes be configured and the server itself started. Below is an example of preparing an express server for startup.

```js
function startServer() {
    if (startInterval) {
        clearInterval(startInterval);
    }
    console.log('Attempting to get access token keys and start server');

    tacUtils.authentication.prepareAuthentication(constants.API_CODE, ACCESS_TOKENS, AUTH_ENABLED)
    .then(() => {
        routes(server);
        server.listen(PORT, function (err) {
            if (err) {
                console.error(err);
            }
            else {
                console.log('Listening on port %s...', PORT);
            }
        });
    })
    .catch((err) => {
        console.error('Unable to start server');
        console.error(err);
        startInterval = setInterval(startServer, startTimeout);
    });
}
```

---

## Web Endpoint Authentication

Specific route endpoints can have authentication enabled by utilising the authentication middleware prior to handling the request:

```js
tacUtils.authentication.authenticate(options)
```

The `options` parameter allows you to override the API code and/or supply an array of 2 character roles that users must have within their system access.

### Authentication with default options

```js
tacUtils.authentication.authenticate()
```

Users must supply a valid Bearer Token and have system access to the default API code specified when preparing authentication.

### Authentication with api code

```js
tacUtils.authentication.authenticate({api: 'AA'})
```

Users must again supply a valid Bearer Token, but this time they need the 'AA' API code as part of their system access rights.

### Authentication with roles

```js
tacUtils.authentication.authenticate({api: 'AA', roles: ['01','02']})
```

Users must again supply a valid Bearer Token, but this time they need the 'AA' API code as part of their system access rights as well as one of the specified roles ('01' or '02').

### Middleware examples

***No authentication***

```js
application.route('/example')
    .get(apiFunctions.getHealth);
```

***Default API authentication***

```js
application.route('/example')
    .get(tacUtils.authentication.authenticate(), apiFunctions.getHealth);
```

***Authentication with override API code***

```js
application.route('/example')
    .get(tacUtils.authentication.authenticate({api: constants.VEHICLE_API_CODE}), apiFunctions.getHealth);
```

***Override API code & Specific Role Auth***

```js
application.route('/example')
    .get(tacUtils.authentication.authenticate({api: constants.VEHICLE_API_CODE, roles: constants.TEST_ROLES}), apiFunctions.getHealth);
```

---

## Socket.IO Authentication

Socket.IO client connections can be required to carry out authentication as part of their connection process. Once authenticated, the socket will remain available for use until closed/re-opened. If authentication expiry is required on socket connections, that must be handled separately to the authentication process (ie. the server must disconnect clients after their expiry time).

The authentication is again carried out as a middleware function, with the same options as the web authentication has available as shown in the example below

```js
io.of('/namespace').use(tacUtils.authentication.socketAuth({roles: ['01','02']}));
```

---

## Authentication Environment Variables

In order for the authentication package to function correctly, environment variables should be set based on the application deployment. Those highlighted in bold should be altered per application.

Environment Variable | Description | Default
--- | --- | ---
**AWS_REGION** | The AWS region to use | eu-west-1

---

[Return to root documentation](./../README.md#tachyon-utilities)