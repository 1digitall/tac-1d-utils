/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * Tachyon metadata utility functions
 *
**/

'use strict'

var jwt = require('jsonwebtoken'),
    aws = require('./awsHelper')
    ;

let AUTH_ACCESS_TOKENS = process.env.AUTH_ACCESS_TOKENS;
let ACCESS_TOKEN_SECRET;
let AUTH_API;
let AUTH_ENABLED = true;

/**
 * @function prepareAuthentication
 * Function to get the relevant keys for authentication and respond to the client whether it is available.
 * @param {string} API - The name of the API
 * @param {string} TOKENS - The name of the access tokens to retrieve from AWS
 * @param {boolean} ENABLED - Whether the authentication should be enabled
**/
exports.prepareAuthentication = async function prepareAuthentication(API, TOKENS, ENABLED) {
    AUTH_API = API.toLowerCase();
    if (TOKENS) {
        AUTH_ACCESS_TOKENS = TOKENS;
    }

    if (ENABLED !== undefined && ENABLED === false) {
        AUTH_ENABLED = false;
    }

    if (!AUTH_ACCESS_TOKENS) {
        throw new Error('AUTH_ACCESS_TOKENS environment variable must be specified to use authentication');
    }

    await getAccessTokens();
}

/**
 * @function authenticate
 * Function to get authenticate incoming request.
**/
exports.authenticate = function authenticate(options) {
    var reqAPI = AUTH_API;
    var reqRoles;
    if (options && options.hasOwnProperty('api')) {
        reqAPI = options.api;
    }
    if (options && options.hasOwnProperty('roles')) {
        reqRoles = options.roles
    }

    return function authenticateRequest(req, res, next) {
        if (!AUTH_ENABLED) {
            req.tacAuthDetails = {
                u: 'unknown'
            };
            return next();
        }
        
        let authHeader = req.headers['authorization'];
        let token = authHeader && authHeader.split(' ')[1];
        if (!token) {
            return res.sendStatus(401);
        }

        jwt.verify(token, ACCESS_TOKEN_SECRET, (err, authDetails) => {
            if (err) {
                return res.sendStatus(403);
            }

            let sa;
            if (reqAPI && authDetails.sa.map(e => e.substr(0, e.indexOf(':')).toUpperCase()).indexOf(reqAPI.toUpperCase()) < 0) {
                return res.sendStatus(401);
            }
            else {
                sa = authDetails.sa.find(e => e.substr(0, e.indexOf(':')).toUpperCase() === reqAPI.toUpperCase());
            }

            if (reqRoles) {
                let userRole = sa.substr(sa.indexOf(':') + 1).toUpperCase();
                if (Array.isArray(reqRoles)) {
                    if (reqRoles.indexOf(userRole) < 0) {
                        return res.sendStatus(401);
                    }
                }
                else if (reqRoles.toUpperCase() !== userRole) {
                    return res.sendStatus(401);
                }
            }

            req.tacAuthDetails = authDetails;
            next();
        });
    }
}

/**
 * @function socketAuth
 * Function to authenticate incoming socket io request.
**/
exports.socketAuth = function socketAuth(options) {
    var reqAPI = AUTH_API;
    var reqRoles;
    if (options && options.hasOwnProperty('api')) {
        reqAPI = options.api;
    }
    if (options && options.hasOwnProperty('roles')) {
        reqRoles = options.roles
    }

    return function authenticateRequest(socket, next) {
        if (!AUTH_ENABLED) {
            socket.tacAuthDetails = {
                u: 'unknown'
            };
            return next();
        }

        if (socket.handshake.query && socket.handshake.query.token) {
            jwt.verify(socket.handshake.query.token, ACCESS_TOKEN_SECRET, (err, authDetails) => {
                if (err) {
                    return next(new Error('Authentication error'));
                }
    
                let sa;
                if (reqAPI && authDetails.sa.map(e => e.substr(0, e.indexOf(':')).toUpperCase()).indexOf(reqAPI.toUpperCase()) < 0) {
                    return next(new Error('Authentication error'));
                }
                else {
                    sa = authDetails.sa.find(e => e.substr(0, e.indexOf(':')).toUpperCase() === reqAPI.toUpperCase());
                }
    
                if (reqRoles) {
                    let userRole = sa.substr(sa.indexOf(':') + 1).toUpperCase();
                    if (Array.isArray(reqRoles)) {
                        if (reqRoles.indexOf(userRole) < 0) {
                            return next(new Error('Authentication error'));
                        }
                    }
                    else if (reqRoles.toUpperCase() !== userRole) {
                        return next(new Error('Authentication error'));
                    }
                }
    
                socket.tacAuthDetails = authDetails;
                next();
            });
        }
        else {
            next(new Error('Authentication error'));
        }
    }
}

/**
 * @function getAccessTokens
 * Obtain access verification tokens
**/
async function getAccessTokens() {
    try {
        let result = await aws.getSecret(AUTH_ACCESS_TOKENS);
        let tokenItem = JSON.parse(result);
        ACCESS_TOKEN_SECRET = tokenItem.access_token;
    }
    catch(ex) {
        console.log(ex);
        throw new Error('Unable to get Secret Manager Authentication Tokens');
    }
}