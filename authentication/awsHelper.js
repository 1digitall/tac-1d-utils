/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * AWS telemetry utility functions
 *
**/

'use strict'

var {
        SecretsManagerClient,
        GetSecretValueCommand
    } = require('@aws-sdk/client-secrets-manager'),
    constants = require('./constants')
    ;

var AWS_REGION = process.env.AWS_REGION || constants.DEFAULT_AWS_REGION;

var secretsManagerClient = new SecretsManagerClient({
    region: AWS_REGION,
    maxAttempts: constants.DEFAULT_AWS_RETRIES
});

/**
 * @function getSecret
 * Obtain AWS secret store value
 * @param {string} name - The secret identifier
**/
exports.getSecret = async function getSecret(name) {
    let params = {
        SecretId: name
    };

    let data = await secretsManagerClient.send(new GetSecretValueCommand(params));
    return data.SecretString;
}