/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * Message severity constants
 *
**/

'use strict'

module.exports = Object.freeze({
    INFORMATION: 'info',
    WARNING: 'warn',
    ERROR: 'error',
    FATAL: 'fatal',
    DEBUG: 'debug'
});