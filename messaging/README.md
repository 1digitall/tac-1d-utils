# Tachyon Messaging

The Tachyon Messaging package is a common utility for all applications to provide system messages in a common structure related to their functionality. 

The package reference allows all modules within an application to interact with a common instance because javascript uses the first in-memory version for all further references. As such, once the messaging is enabled (via environment variable), any modules can submit messages.

---

## Enable Messaging

The Tachyon Messaging package is disabled by default. To enable the package within an application the environment variable MESSAGING_ENABLED must be set to true:

```js
process.env.MESSAGING_ENABLED = true;
```

If messaging is not enabled, the package will still accept the submission of message data, but nothing will be delivered to the system data stream.

---

## Submit Message

```js
tacUtils.messaging.submitMessage(clientId, source, module, type, severity, payload, context, pipe);
```

Parameter | Type | Required | Information
--- | --- | --- | --- | ---
clientId | string | Yes | The clientId related to the message
source | string | Yes | The source application
module | string | Yes | The application module
type | string | Yes | The message type - one of tacUtils.messaging.MessageTypes
severity | string | Yes | The message severity - one of tacUtils.messaging.MessageSeverity
payload | object | Yes | The message object
context | string | No | Context to the message - e.g. pipeline
pipe | object | No | Pipeline related data if applicable

---

## Messaging Environment Variables

In order for the messaging package to function correctly, environment variables should be set based on the application deployment. Those highlighted in bold should be altered per application.

Environment Variable | Description | Default
--- | --- | ---
MESSAGING_INTERVAL | The interval (in milliseconds) on which messages are delivered to kinesis | 500
**MESSAGING_ENABLED** | Whether messaging is enabled for an application | false
**TELEMETRY_STREAM_NAME** | The name of the system kinesis stream | tac-1d-tdata01-system
**TELEMETRY_DELIVERY_ROLE** | The delivery role to provide access to the kinesis stream | 
DATA_RELOAD | Time in minutes between refreshing the delivery role access | 30
DATA_DURATION | Time in minutes that the delivery role should be assumed for | 60
**AWS_REGION** | The AWS region to use | eu-west-1
AWS_SOCKET_TIMEOUT | Socket timeout (in milliseconds) on AWS resources | 60000
AWS_RETRIES | Number of retries on AWS resources | 5

---

[Return to root documentation](./../README.md#tachyon-utilities)