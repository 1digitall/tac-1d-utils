/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * Messaging constants
 *
**/

'use strict'

module.exports = Object.freeze({
    DEFAULT_MESSAGING_ENABLED: false,
    DEFAULT_MESSAGING_INTERVAL: 500,
    DEFAULT_SYSTEM_STREAM_NAME: 'tac-1d-tdata01-system',
    DEFAULT_SYSTEM_DELIVERY_ROLE: '',
    DEFAULT_AWS_REGION: 'eu-west-1',
    DEFAULT_AWS_RETRIES: 5,
    DEFAULT_DATA_RELOAD: 30,
    DEFAULT_DATA_DURATION: 60
});