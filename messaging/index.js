/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * Tachyon messaging functionality
 *
**/

'use strict'

var uuid = require('uuid'),
    aws = require('./awsHelper'),
    msgSeverity = require('./msg-severity'),
    msgTypes = require('./msg-types'),
    constants = require('./constants')
    ;

var MESSAGING_INTERVAL = process.env.MESSAGING_INTERVAL || constants.DEFAULT_MESSAGING_INTERVAL;
var MESSAGING_ENABLED = constants.DEFAULT_MESSAGING_ENABLED;
if (process.env.MESSAGING_ENABLED) {
    MESSAGING_ENABLED = (process.env.MESSAGING_ENABLED === 'true');
} 
var SYSTEM_STREAM_NAME = process.env.TELEMETRY_STREAM_NAME || constants.DEFAULT_SYSTEM_STREAM_NAME;

var messages = [];
var deliveryInterval;

if (MESSAGING_ENABLED) {
    aws.createKinesisConnection();
    deliverMessages();
}

/**
 * @function deliverMessages
 * Function to deliver message data to the system stream
**/
async function deliverMessages() {
    if (messages.length === 0) {
        if (!deliveryInterval) {
            deliveryInterval = setInterval(deliverMessages, MESSAGING_INTERVAL);
        }
        return;
    }

    if (deliveryInterval) {
        clearInterval(deliveryInterval);
    }

    let deliveries = JSON.parse(JSON.stringify(messages));

    try {
        await aws.putBatchKinesis(SYSTEM_STREAM_NAME, deliveries);
        for (var i = 0; i < deliveries.length; i++) {
            messages.shift();
        }
        deliveryInterval = setInterval(deliverMessages, MESSAGING_INTERVAL);
    }
    catch(ex) {
        deliveryInterval = setInterval(deliverMessages, MESSAGING_INTERVAL);
    }
}

/**
 * @function submitMessage
 * Function to submit message data
 * @param {string} clientId - (Required) The clientId
 * @param {string} source - (Required) The source application
 * @param {string} module - (Required) The application module
 * @param {string} type - (Required) The type of message (error, status, progress etc)
 * @param {string} severity - (Required) The message severity (info, warn etc)
 * @param {object} payload - (Required) The message data
 * @param {string} context - (Optional) The message context
 * @param {object} pipe - (Optional) The pipe connection object details
**/
exports.submitMessage = function submitMessage(clientId, source, module, type, severity, payload, context, pipe) {
    if (!MESSAGING_ENABLED) {
        return;
    }

    if (!clientId || !source || !module || !type || !severity || !payload) {
        throw new Error('Required message data missing');
    }
    if (Object.values(msgTypes).indexOf(type.toLowerCase()) < 0) {
        throw new Error('Message type is invalid');
    }
    else {
        type = type.toLowerCase();
    }
    if (Object.values(msgSeverity).indexOf(severity.toLowerCase()) < 0) {
        throw new Error('Message severity is invalid');
    }
    else {
        severity = severity.toLowerCase();
    }

    if (!context) {
        context = '';
    }
    if (!pipe) {
        pipe = {};
    }

    let msg = {
        messageId: uuid.v4(),
        context: context,
        clientId: clientId,
        pipe: pipe,
        source: source,
        module: module,
        type: type,
        severity: severity,
        created: (new Date()).toISOString(),
        payload: payload
    };

    messages.push(msg);
}

exports.MessageSeverity = msgSeverity;
exports.MessageTypes = msgTypes;