/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * Message type constants
 **
*/

'use strict'

module.exports = Object.freeze({
    ERROR: 'error',
    STATUS: 'status',
    PROGRESS: 'progress',
    UPDATE: 'update',
    SYSTEM: 'system',
    NOTIFICATION: 'notification'
});