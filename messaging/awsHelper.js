/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * AWS messaging utility functions
 *
**/

'use strict'

var {
        KinesisClient,
        PutRecordsCommand
    } = require('@aws-sdk/client-kinesis'),
    {
        STSClient,
        AssumeRoleCommand
    } = require('@aws-sdk/client-sts'),
    uuid = require('uuid'),
    constants = require('./constants')
    ;


var AWS_REGION = process.env.AWS_REGION || constants.DEFAULT_AWS_REGION;
var AWS_RETRIES = constants.DEFAULT_AWS_RETRIES;
if (!isNaN(process.env.AWS_RETRIES)) {
    AWS_RETRIES = parseInt(process.env.AWS_RETRIES);
}

var SYSTEM_DELIVERY_ROLE = process.env.TELEMETRY_DELIVERY_ROLE || constants.DEFAULT_SYSTEM_DELIVERY_ROLE;
var DATA_RELOAD = constants.DEFAULT_DATA_RELOAD;
if (!isNaN(process.env.DATA_RELOAD)) {
    DATA_RELOAD = parseInt(process.env.DATA_RELOAD);
}
DATA_RELOAD = DATA_RELOAD * 1000 * 60;
var DATA_DURATION = constants.DEFAULT_DATA_DURATION;
if (!isNaN(process.env.DATA_DURATION)) {
    DATA_DURATION = parseInt(process.env.DATA_DURATION);
}
DATA_DURATION = DATA_DURATION * 60;

var kinesis;
var dataCredsInterval;

/**
 * @function createKinesisConnection
 * Establish kinesis connection based on credentials configuration
**/
exports.createKinesisConnection = function createKinesisConnection() {
    if (SYSTEM_DELIVERY_ROLE && SYSTEM_DELIVERY_ROLE !== '') {
        reloadDataCredentials();
    }
    else {
        kinesis = new KinesisClient({
            region: AWS_REGION,
            maxAttempts: AWS_RETRIES
        });
    }
}

/**
 * @function reloadDataCredentials
 * Reloads Kinesis Credentials based on DATA_RELOAD
**/
async function reloadDataCredentials() {
    if (dataCredsInterval) {
        clearInterval(dataCredsInterval);
    }
    let errRetry = 1000 * 60 * 2;
    let stsClient = new STSClient();
    let stsData;
    let params = {
        RoleSessionName: `Messaging-Session-${(new Date()).getTime()}`,
        RoleArn: SYSTEM_DELIVERY_ROLE,
        DurationSeconds: DATA_DURATION
    };

    try {
        stsData = await stsClient.send(new AssumeRoleCommand(params));
    }
    catch(ex) {
        console.error('Unable to asssume data delivery role ' + SYSTEM_DELIVERY_ROLE);
        console.error(ex);
        dataCredsInterval = setInterval(reloadDataCredentials, errRetry);
        return;
    }

    kinesis = new KinesisClient({
        region: AWS_REGION,
        credentials: {
            accessKeyId: stsData.Credentials.AccessKeyId,
            secretAccessKey: stsData.Credentials.SecretAccessKey,
            sessionToken: stsData.Credentials.SessionToken
        },
        maxAttempts: AWS_RETRIES
    });

    dataCredsInterval = setInterval(reloadDataCredentials, DATA_RELOAD);
}

/**
 * @function putBatchKinesis
 * Function to put array of data into Kinesis stream
 * @param {string} streamName - The kinesis stream name
 * @param {array} kinesisData - The object array of data to be added
**/
exports.putBatchKinesis = async function putBatchKinesis(streamName, kinesisData) {
    if (!kinesis) {
        throw new Error('Kinesis messaging client not initialised');
    }

    let kinesisRecords = [];
    for (var i = 0; i < kinesisData.length; i++) {
        let dataItem = kinesisData[i];
        let record = {
            Data: Buffer.from(JSON.stringify(dataItem) + '\n'),
            PartitionKey: uuid.v4()
        };
        kinesisRecords.push(record);
    }

    let params = {
        Records: kinesisRecords,
        StreamName: streamName
    };

    await kinesis.send(new PutRecordsCommand(params));
}