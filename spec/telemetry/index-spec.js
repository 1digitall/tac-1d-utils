/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * Unit testing for telemetry/index.js
 * See https://www.npmjs.com/package/jasmine-node for jasmine details
 *
 */

'use strict'

var rewire = require('rewire'),
    telemetry = rewire('./../../telemetry/index')
    ;

// startTelemetry
describe('Start collecting telemetry data', function() {
    it('should successfully start telemetry processing', function(done) {
        let packageName = 'test-app';
        let packageVersion = '0.1.0';

        let _telConfig = {
            getTelemetryFields: function() {
                return new Promise((resolve) => {
                    resolve(['pipeline', 'eventName']);
                });
            }
        };
        let _setInterval = function(func, interval) {
            if (func.toString().indexOf('awsFunctions.putKinesis') > 0) {
                expect(console.warn).toHaveBeenCalled();

                revertConfig();
                revertInterval();
                done();
            }
        };

        var revertConfig = telemetry.__set__('telConfig', _telConfig);
        var revertInterval = telemetry.__set__('setInterval', _setInterval);
        spyOn(console, 'warn');
        telemetry.startTelemetry(packageName, packageVersion);
    });
});

// addTelEvent
describe('Add successful event to telemetry', function() {
    it('should successfully add to the telemetry events', function(done) {
        let _TELEMETRY_FIELDS = ['pipeline', 'eventName'];
        let _telEvents = {
            events: {
                push: function(item) {
                    expect(item).toBeDefined();
                    expect(item.pipeline).toBeDefined();
                    expect(item.eventName).toBeDefined();
                    expect(item.source).not.toBeDefined();
                    expect(item.pipeline).toEqual('test');
                    expect(item.eventName).toEqual('testevent');
                    done();
                }
            }
        };

        let metadata = {
            pipeline: 'Test',
            source: 'Jasmine',
            eventName: 'TestEvent',
            version: 1
        };

        var revertFields = telemetry.__set__('TELEMETRY_FIELDS', _TELEMETRY_FIELDS);
        var revertEvents = telemetry.__set__('telEvents', _telEvents);

        telemetry.addTelEvent(metadata);

        revertFields();
        revertEvents();
    });

    it('should successfully add to the telemetry events regardless of attribute case', function(done) {
        let _TELEMETRY_FIELDS = ['pipeline', 'eventName'];
        let _telEvents = {
            events: {
                push: function(item) {
                    expect(item).toBeDefined();
                    expect(item.pipeline).toBeDefined();
                    expect(item.eventName).toBeDefined();
                    expect(item.source).not.toBeDefined();
                    expect(item.pipeline).toEqual('test');
                    expect(item.eventName).toEqual('testevent');
                    done();
                }
            }
        };

        let metadata = {
            Pipeline: 'Test',
            source: 'Jasmine',
            EventName: 'TestEvent',
            version: 1
        };

        var revertFields = telemetry.__set__('TELEMETRY_FIELDS', _TELEMETRY_FIELDS);
        var revertEvents = telemetry.__set__('telEvents', _telEvents);

        telemetry.addTelEvent(metadata);

        revertFields();
        revertEvents();
    });

    it('should add empty data for missing telemetry fields', function(done) {
        let _TELEMETRY_FIELDS = ['pipeline', 'eventName'];
        let _telEvents = {
            events: {
                push: function(item) {
                    expect(item).toBeDefined();
                    expect(item.pipeline).toBeDefined();
                    expect(item.eventName).toBeDefined();
                    expect(item.source).not.toBeDefined();
                    expect(item.pipeline).toEqual('');
                    expect(item.eventName).toEqual('testevent');
                    done();
                }
            }
        };

        let metadata = {
            source: 'Jasmine',
            eventName: 'TestEvent',
            version: 1
        };

        var revertFields = telemetry.__set__('TELEMETRY_FIELDS', _TELEMETRY_FIELDS);
        var revertEvents = telemetry.__set__('telEvents', _telEvents);

        telemetry.addTelEvent(metadata);

        revertFields();
        revertEvents();
    });
});

// addTelPII
describe('Add successful pii to telemetry', function() {
    it('should successfully add to the telemetry pii', function(done) {
        let _TELEMETRY_FIELDS = ['pipeline', 'eventName'];
        let _telEvents = {
            pii: {
                push: function(item) {
                    expect(item).toBeDefined();
                    expect(item.pipeline).toBeDefined();
                    expect(item.eventName).toBeDefined();
                    expect(item.source).not.toBeDefined();
                    expect(item.pipeline).toEqual('test');
                    expect(item.eventName).toEqual('testevent');
                    done();
                }
            }
        };

        let metadata = {
            pipeline: 'Test',
            source: 'Jasmine',
            eventName: 'TestEvent',
            version: 1
        };

        var revertFields = telemetry.__set__('TELEMETRY_FIELDS', _TELEMETRY_FIELDS);
        var revertEvents = telemetry.__set__('telEvents', _telEvents);

        telemetry.addTelPII(metadata);

        revertFields();
        revertEvents();
    });

    it('should successfully add to the telemetry pii regardless of attribute case', function(done) {
        let _TELEMETRY_FIELDS = ['pipeline', 'eventName'];
        let _telEvents = {
            pii: {
                push: function(item) {
                    expect(item).toBeDefined();
                    expect(item.pipeline).toBeDefined();
                    expect(item.eventName).toBeDefined();
                    expect(item.source).not.toBeDefined();
                    expect(item.pipeline).toEqual('test');
                    expect(item.eventName).toEqual('testevent');
                    done();
                }
            }
        };

        let metadata = {
            Pipeline: 'Test',
            source: 'Jasmine',
            EventName: 'TestEvent',
            version: 1
        };

        var revertFields = telemetry.__set__('TELEMETRY_FIELDS', _TELEMETRY_FIELDS);
        var revertEvents = telemetry.__set__('telEvents', _telEvents);

        telemetry.addTelPII(metadata);

        revertFields();
        revertEvents();
    });

    it('should add empty data for missing telemetry fields', function(done) {
        let _TELEMETRY_FIELDS = ['pipeline', 'eventName'];
        let _telEvents = {
            pii: {
                push: function(item) {
                    expect(item).toBeDefined();
                    expect(item.pipeline).toBeDefined();
                    expect(item.eventName).toBeDefined();
                    expect(item.source).not.toBeDefined();
                    expect(item.pipeline).toEqual('');
                    expect(item.eventName).toEqual('testevent');
                    done();
                }
            }
        };

        let metadata = {
            source: 'Jasmine',
            eventName: 'TestEvent',
            version: 1
        };

        var revertFields = telemetry.__set__('TELEMETRY_FIELDS', _TELEMETRY_FIELDS);
        var revertEvents = telemetry.__set__('telEvents', _telEvents);

        telemetry.addTelPII(metadata);

        revertFields();
        revertEvents();
    });
});

// addTelError
describe('Add successful error to telemetry', function() {
    it('should successfully add to the telemetry errors', function(done) {
        let _TELEMETRY_FIELDS = ['pipeline', 'eventName'];
        let _telEvents = {
            errors: {
                push: function(item) {
                    expect(item).toBeDefined();
                    expect(item.pipeline).toBeDefined();
                    expect(item.eventName).toBeDefined();
                    expect(item.source).not.toBeDefined();
                    expect(item.pipeline).toEqual('test');
                    expect(item.eventName).toEqual('testevent');
                    done();
                }
            }
        };

        let metadata = {
            pipeline: 'Test',
            source: 'Jasmine',
            eventName: 'TestEvent',
            version: 1
        };

        var revertFields = telemetry.__set__('TELEMETRY_FIELDS', _TELEMETRY_FIELDS);
        var revertEvents = telemetry.__set__('telEvents', _telEvents);

        telemetry.addTelError(metadata);

        revertFields();
        revertEvents();
    });

    it('should successfully add to the telemetry errors regardless of attribute case', function(done) {
        let _TELEMETRY_FIELDS = ['pipeline', 'eventName'];
        let _telEvents = {
            errors: {
                push: function(item) {
                    expect(item).toBeDefined();
                    expect(item.pipeline).toBeDefined();
                    expect(item.eventName).toBeDefined();
                    expect(item.source).not.toBeDefined();
                    expect(item.pipeline).toEqual('test');
                    expect(item.eventName).toEqual('testevent');
                    done();
                }
            }
        };

        let metadata = {
            Pipeline: 'Test',
            source: 'Jasmine',
            EventName: 'TestEvent',
            version: 1
        };

        var revertFields = telemetry.__set__('TELEMETRY_FIELDS', _TELEMETRY_FIELDS);
        var revertEvents = telemetry.__set__('telEvents', _telEvents);

        telemetry.addTelError(metadata);

        revertFields();
        revertEvents();
    });

    it('should add empty data for missing telemetry fields', function(done) {
        let _TELEMETRY_FIELDS = ['pipeline', 'eventName'];
        let _telEvents = {
            errors: {
                push: function(item) {
                    expect(item).toBeDefined();
                    expect(item.pipeline).toBeDefined();
                    expect(item.eventName).toBeDefined();
                    expect(item.source).not.toBeDefined();
                    expect(item.pipeline).toEqual('');
                    expect(item.eventName).toEqual('testevent');
                    done();
                }
            }
        };

        let metadata = {
            source: 'Jasmine',
            eventName: 'TestEvent',
            version: 1
        };

        var revertFields = telemetry.__set__('TELEMETRY_FIELDS', _TELEMETRY_FIELDS);
        var revertEvents = telemetry.__set__('telEvents', _telEvents);

        telemetry.addTelError(metadata);

        revertFields();
        revertEvents();
    });
});

// condenseTelemetryArray
describe('Combine common telemetry items with corresponding totals', function() {
    it('should successfully condense telemetry array data', function(done) {
        let eventData = [
            {
                pipeline: 'Test',
                source: 'Jasmine',
                eventName: 'TestEvent'
            },
            {
                pipeline: 'Test',
                source: 'Jasmine',
                eventName: 'TestEvent'
            },
            {
                pipeline: 'Test',
                source: 'Jasmine',
                eventName: 'TestEvent'
            },
            {
                pipeline: 'Test2',
                source: 'Jasmine2',
                eventName: 'TestEvent2'
            },
            {
                pipeline: 'Test2',
                source: 'Jasmine2',
                eventName: 'TestEvent2'
            }
        ];

        var condensed = telemetry.condenseTelemetryArray(eventData);

        expect(condensed).toBeDefined();
        expect(condensed.length).toEqual(2);
        expect(condensed[0].pipeline).toEqual('Test');
        expect(condensed[1].pipeline).toEqual('Test2');
        expect(condensed[0].source).toEqual('Jasmine');
        expect(condensed[1].source).toEqual('Jasmine2');
        expect(condensed[0].eventName).toEqual('TestEvent');
        expect(condensed[1].eventName).toEqual('TestEvent2');
        expect(condensed[0].total).toEqual(3);
        expect(condensed[1].total).toEqual(2);
        done();
    });

    it('should be able to handle empty array', function(done) {
        let eventData = [];
        var condensed = telemetry.condenseTelemetryArray(eventData);
        expect(condensed).toBeDefined();
        expect(condensed).toEqual([]);
        done();
    });
});

// buildTelemetry
describe('Combine common telemetry items with corresponding totals', function() {
    it('should create telemetry object', function(done) {
        let telData = telemetry.buildTelemetry();
        expect(telData).toBeDefined();
        expect(telData.metadata).toBeDefined();
        expect(telData.metadata.pipeline).toEqual('telemetry');
        expect(telData.metadata.eventName).toEqual('telemetry');
        expect(telData.metadata.eventId).toBeDefined();
        expect(telData.metadata.submittedAt).toBeDefined();
        expect(telData.payload).toBeDefined();
        expect(telData.payload.app).toBeDefined();
        expect(telData.payload.perf).toBeDefined();
        expect(telData.payload.endTime).toBeDefined();
        expect(telData.payload.events).toBeDefined();
        expect(telData.payload.pii).toBeDefined();
        expect(telData.payload.errors).toBeDefined();
        expect(telData.payload.totalEvents).toBeDefined();
        expect(telData.payload.totalEvents).toEqual(0);
        expect(telData.payload.totalPII).toBeDefined();
        expect(telData.payload.totalPII).toEqual(0);
        expect(telData.payload.totalErrors).toBeDefined();
        expect(telData.payload.totalErrors).toEqual(0);
        
        done();
    });
});