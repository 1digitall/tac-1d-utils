/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * Unit testing for telemetry/task-helper.js
 * See https://www.npmjs.com/package/jasmine-node for jasmine details
 *
 */

'use strict'

var rewire = require('rewire'),
    taskFunctions = rewire('./../../telemetry/task-helper')
    ;

// getContainerData
describe('Use container metadata to enhance application telemetry', function() {
    it('should fully populate the app object with container information', function(done) {
        let _metadata_path = 'unpopulated';
        let _fs = {
            readFileSync: function() {
                let metadata = {
                    MetadataFileStatus: 'READY',
                    Cluster: 'ECS Cluster',
                    ContainerName: 'ECS Container Name',
                    ImageName: 'ECS Image Name',
                    TaskARN: 'arn:ecs/tac-1d-tpri01-task'
                };
                return JSON.stringify(metadata);
            }
        };

        let packageName = 'package-name';
        let packageVersion = '0.0.1';

        var revertPath = taskFunctions.__set__('METADATA_PATH', _metadata_path);
        var revertFS = taskFunctions.__set__('fs', _fs);
        taskFunctions.getContainerData(packageName, packageVersion)
        .then((telApp) => {
            expect(telApp).toBeDefined();
            expect(telApp.name).toBeDefined();
            expect(telApp.name).toEqual(packageName);
            expect(telApp.version).toBeDefined();
            expect(telApp.version).toEqual(packageVersion);
            expect(telApp.cluster).toBeDefined();
            expect(telApp.cluster).toEqual('ECS Cluster');
            expect(telApp.containerName).toBeDefined();
            expect(telApp.containerName).toEqual('ECS Container Name');
            expect(telApp.imageName).toBeDefined();
            expect(telApp.imageName).toEqual('ECS Image Name');
            expect(telApp.taskID).toBeDefined();
            expect(telApp.taskID).toEqual('tac-1d-tpri01-task');

            revertPath();
            revertFS();
            done();
        });
    });

    it('should provide just the name & version if container path is not set', function(done) {
        let packageName = 'package-name';
        let packageVersion = '0.0.1';
        spyOn(console, 'warn');

        taskFunctions.getContainerData(packageName, packageVersion)
        .then((telApp) => {
            expect(telApp).toBeDefined();
            expect(telApp.name).toBeDefined();
            expect(telApp.name).toEqual(packageName);
            expect(telApp.version).toBeDefined();
            expect(telApp.version).toEqual(packageVersion);
            expect(telApp.cluster).toBeDefined();
            expect(telApp.cluster).toEqual('');
            expect(telApp.containerName).toBeDefined();
            expect(telApp.containerName).toEqual('');
            expect(telApp.imageName).toBeDefined();
            expect(telApp.imageName).toEqual('');
            expect(telApp.taskID).toBeDefined();
            expect(telApp.taskID).toEqual('');
            expect(console.warn).toHaveBeenCalled();
            done();
        });
    });

    it('should retry and pick up container path if not available immediately', function(done) {
        let _metadata_path = 'unpopulated';
        let _retry_interval = 1000;
        let _fs = {
            readFileSync: function() {
                let metadata = {
                    MetadataFileStatus: 'NOT READY',
                    Cluster: '',
                    ContainerName: '',
                    ImageName: '',
                    TaskARN: ''
                };
                return JSON.stringify(metadata);
            }
        };

        let packageName = 'package-name';
        let packageVersion = '0.0.1';

        var revertPath = taskFunctions.__set__('METADATA_PATH', _metadata_path);
        var revertInterval = taskFunctions.__set__('RETRY_INTERVAL', _retry_interval);
        var revertFS = taskFunctions.__set__('fs', _fs);
        spyOn(console, 'warn');

        setTimeout(function() {
            _fs.readFileSync = function() {
                let metadata = {
                    MetadataFileStatus: 'READY',
                    Cluster: 'ECS Cluster',
                    ContainerName: 'ECS Container Name',
                    ImageName: 'ECS Image Name',
                    TaskARN: 'arn:ecs/tac-1d-tpri01-task'
                };
                return JSON.stringify(metadata);
            };
        }, 500);

        taskFunctions.getContainerData(packageName, packageVersion)
        .then((telApp) => {
            expect(telApp).toBeDefined();
            expect(telApp.name).toBeDefined();
            expect(telApp.name).toEqual(packageName);
            expect(telApp.version).toBeDefined();
            expect(telApp.version).toEqual(packageVersion);
            expect(telApp.cluster).toBeDefined();
            expect(telApp.cluster).toEqual('ECS Cluster');
            expect(telApp.containerName).toBeDefined();
            expect(telApp.containerName).toEqual('ECS Container Name');
            expect(telApp.imageName).toBeDefined();
            expect(telApp.imageName).toEqual('ECS Image Name');
            expect(telApp.taskID).toBeDefined();
            expect(telApp.taskID).toEqual('tac-1d-tpri01-task');
            expect(console.warn).toHaveBeenCalled();

            revertPath();
            revertInterval();
            revertFS();
            done();
        });
    });
});