/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * Unit testing for telemetry/sysinfo-helper.js
 * See https://www.npmjs.com/package/jasmine-node for jasmine details
 *
 */

'use strict'

var sysinfoFunctions = require('./../../telemetry/sysinfo-helper')
    ;

// buildPerformance
describe('Build system performance data', function() {
    it('should return system information', async function(done) {
        await capturePerf();

        var perf = sysinfoFunctions.buildPerformance();
        expect(perf).toBeDefined();
        expect(perf.cpu).toBeDefined();
        expect(perf.cpu.load).toBeDefined();
        expect(perf.cpu.idle).toBeDefined();
        expect(perf.mem).toBeDefined();
        expect(perf.mem.total).toBeDefined();
        expect(perf.mem.used).toBeDefined();
        expect(perf.mem.free).toBeDefined();
        expect(perf.network).toBeDefined();
        expect(perf.network.receive).toBeDefined();
        expect(perf.network.receive.data).toBeDefined();
        expect(perf.network.receive.errors).toBeDefined();
        expect(perf.network.receive.dropped).toBeDefined();
        expect(perf.network.send).toBeDefined();
        expect(perf.network.send.data).toBeDefined();
        expect(perf.network.send.errors).toBeDefined();
        expect(perf.network.send.dropped).toBeDefined();
        expect(perf.disk).toBeDefined();
        expect(perf.disk.io).toBeDefined();
        expect(perf.disk.io.read).toBeDefined();
        expect(perf.disk.io.write).toBeDefined();
        expect(perf.disk.io.total).toBeDefined();
        expect(perf.disk.transfer).toBeDefined();
        expect(perf.disk.transfer.read).toBeDefined();
        expect(perf.disk.transfer.write).toBeDefined();
        expect(perf.disk.transfer.total).toBeDefined();
        done(); 
    });
});

function capturePerf() {
    return new Promise((resolve) => {
        callPerf(0);

        function callPerf(i) {
            sysinfoFunctions.capturePerformance();
            setTimeout(function() {
                i++;
                if (i > 2) {
                    resolve('done');
                }
                else {
                    callPerf(i);
                }
            }, 500);
        }
    });
}