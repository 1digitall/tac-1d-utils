/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * Unit testing for telemetry/aws-helper.js
 * See https://www.npmjs.com/package/jasmine-node for jasmine details
 *
 */

'use strict'

var rewire = require('rewire'),
    awsFunctions = rewire('./../../telemetry/aws-helper')
    ;

// getDynamo
describe('Get item from dynamoDB using partition key', function() {
    it('should get an item for the partition keys', function(done) {
        let _dynamoDBClient = {
            get: function (params, callback) {
                let compareParams = {
                    TableName: 'tableName',
                    Key: {
                        partKey: 'partitionKey',
                        sortKey: 'secondaryKey'
                    }
                };
                expect(params).toEqual(compareParams);
                let returnData = {
                    Item: {
                            partKey: 'partitionKey',
                            sortKey: 'secondaryKey',
                            otherData: 'otherData'
                    }
                };
                callback(null, returnData);
            }
        };

        let tableName = 'tableName';
        let tableKeys = [];
        tableKeys.push({
            name: 'partKey',
            value: 'partitionKey'
        });
        tableKeys.push({
            name: 'sortKey',
            value: 'secondaryKey'
        });

        var revert = awsFunctions.__set__('dynamoDBClient', _dynamoDBClient);
        awsFunctions.getDynamo(tableName, tableKeys)
        .then((result) => {
            expect(result).toBeDefined();
            expect(result.partKey).toEqual('partitionKey');
            expect(result.sortKey).toEqual('secondaryKey');
            revert();
            done();            
        })
        .catch((err) => {
            expect(true).toBeFalsy();
            console.error(err);
            revert();
            done();
        });
    });
});

// putKinesis
describe('Save data to Kinesis Stream', function() {
    it('should add the data item to kinesis', function(done) {
        let _kinesis = {
            putRecord: function (params, callback) {
                expect(params.Data).toBeDefined();
                expect(params.Data.toString('utf-8').indexOf('some data')).not.toBeLessThan(0);
                expect(params.StreamName).toEqual('kinesisName');
                expect(params.PartitionKey).toBeDefined();
                callback(null);
                done();
            }
        };

        let streamName = 'kinesisName';
        let dataItem = {
            payload: 'some data'
        };

        var revert = awsFunctions.__set__('kinesis', _kinesis);
        awsFunctions.putKinesis(streamName, dataItem);
        revert();
    });
});