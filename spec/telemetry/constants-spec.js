/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * Unit testing for telemetry/constants.js
 * See https://www.npmjs.com/package/jasmine-node for jasmine details
 *
 */

'use strict'

var constants = require('./../../telemetry/constants')
    ;

// check defaults
describe('Check that default variables are set', function() {
    it('should have populated variables', function(done) {
        expect(constants.DEFAULT_TELEMETRY_ENABLED).toBeDefined();
        expect(constants.DEFAULT_TELEMETRY_INTERVAL).toBeDefined();
        expect(constants.DEFAULT_TELEMETRY_CONFIG_RETRY_INTERVAL).toBeDefined();
        expect(constants.DEFAULT_TELEMETRY_TABLE).toBeDefined();
        expect(constants.DEFAULT_TELEMETRY_TABLE_KEY).toBeDefined();
        expect(constants.DEFAULT_TELEMETRY_STREAM_NAME).toBeDefined();
        expect(constants.DEFAULT_TELEMETRY_CONTROL_ENDPOINT).toBeDefined();
        expect(constants.DEFAULT_TELEMETRY_FIELDS).toBeDefined();
        expect(constants.DEFAULT_AWS_REGION).toBeDefined();
        expect(constants.DEFAULT_AWS_SOCKET_TIMEOUT).toBeDefined();
        expect(constants.DEFAULT_AWS_RETRIES).toBeDefined();
        done();
    });
});