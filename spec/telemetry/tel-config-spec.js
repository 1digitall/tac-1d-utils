/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * Unit testing for telemetry/tel-config.js
 * See https://www.npmjs.com/package/jasmine-node for jasmine details
 *
 */

'use strict'

var rewire = require('rewire'),
    telConfig = rewire('./../../telemetry/tel-config')
    ;

// getTelemetryFields
describe('Get fields for incoming metadata object use in telemetry', function() {
    it('should successfully get the local telemetry fields', function(done) {
        let _TELEMETRY_TABLE = 'test-table';
        let _TELEMETRY_TABLE_KEY = 'telemetryKey';

        let _awsFunctions = {
            getDynamo: function(tableName, tableKeys) {
                return new Promise((resolve, reject) => {
                    expect(tableName).toEqual('test-table');
                    expect(tableKeys).toEqual([{
                        name: 'telKey',
                        value: 'telemetryKey'
                    }]);

                    let resultData = {
                        telKey: 'telemetryKey',
                        telFields: [
                            'pipeline',
                            'eventName'
                        ]
                    };
                    
                    resolve(resultData);
                });
            }
        };

        var revertTable = telConfig.__set__('TELEMETRY_TABLE', _TELEMETRY_TABLE);
        var revertKey = telConfig.__set__('TELEMETRY_TABLE_KEY', _TELEMETRY_TABLE_KEY);
        var revertAWS = telConfig.__set__('awsFunctions', _awsFunctions);

        telConfig.getTelemetryFields()
        .then((result) => {
            expect(result).toBeDefined();
            expect(result).toEqual(['pipeline', 'eventName']);

            revertTable();
            revertKey();
            revertAWS();
            done();
        });
    });

    it('should provide default telemetry fields if local db unavailable', function(done) {
        let _TELEMETRY_TABLE = 'test-table';
        let _TELEMETRY_TABLE_KEY = 'telemetryKey';

        let _awsFunctions = {
            getDynamo: function(tableName, tableKeys) {
                return new Promise((resolve, reject) => {
                    expect(tableName).toEqual('test-table');
                    expect(tableKeys).toEqual([{
                        name: 'telKey',
                        value: 'telemetryKey'
                    }]);
                    
                    reject('AWS Error');
                });
            }
        };

        var revertTable = telConfig.__set__('TELEMETRY_TABLE', _TELEMETRY_TABLE);
        var revertKey = telConfig.__set__('TELEMETRY_TABLE_KEY', _TELEMETRY_TABLE_KEY);
        var revertAWS = telConfig.__set__('awsFunctions', _awsFunctions);
        spyOn(console, 'warn');

        telConfig.getTelemetryFields()
        .then((result) => {
            expect(result).toBeDefined();
            expect(result).toEqual(['pipeline', 'source', 'eventName']);
            expect(console.warn).toHaveBeenCalled();

            revertTable();
            revertKey();
            revertAWS();
            done();
        });
    });
});