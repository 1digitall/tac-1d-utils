/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * Unit testing for metadata/index.js
 * See https://www.npmjs.com/package/jasmine-node for jasmine details
 *
 */

'use strict'

var metadataFunctions = require('./../../metadata/index'),
    uuid = require('uuid')
    ;

// buildMetadata
describe('Create standard Tachyon metadata object', function() {
    it('should provide a complete metadata object for given input values', function(done) {
        let testPipeline = 'Website';
        let testSource = 'Vivid';
        let testEventName = 'TestEvent';
        let testSchemaVersion = 1;
        let testExternalId = uuid.v4();
        let testBrand = 'TestBrand';
        let testLocale = 'en_BORK';
        let testCountry = 'RSA';
        let testService = 'TestService';
        let testModule = 'TestModule';
        let testPD = true;
        let testSC = 'public';
        let testIngestion = 'single';
        let testDiscover = true;
        let testTags = [
            {
                name: 'TestName',
                value: 'TestValue'
            }
        ];
        let testSynthetic = false;

        let metadata = metadataFunctions.buildMetadata(testPipeline, testSource, testEventName, testSchemaVersion, testExternalId, 
            testBrand, testLocale, testCountry, testService, testModule, testPD, testSC, testIngestion, testDiscover, testTags, testSynthetic);
        
        expect(metadata).toBeDefined();
        expect(metadata.pipeline).toEqual(testPipeline);
        expect(metadata.source).toEqual(testSource);
        expect(metadata.eventName).toEqual(testEventName);
        expect(metadata.version).toEqual(testSchemaVersion);
        expect(metadata.externalId).toEqual(testExternalId);
        expect(metadata.brand).toEqual(testBrand);
        expect(metadata.locale).toEqual(testLocale);
        expect(metadata.country).toEqual(testCountry);
        expect(metadata.service).toEqual(testService);
        expect(metadata.module).toEqual(testModule);
        expect(metadata.pd).toEqual(testPD);
        expect(metadata.sc).toEqual(testSC);
        expect(metadata.ingestionType).toEqual(testIngestion);
        expect(metadata.discoverable).toEqual(testDiscover);
        expect(metadata.tags).toEqual(testTags);
        expect(metadata.synthetic).toEqual(testSynthetic);
        expect(metadata.occurredAt).toBeDefined();
        expect(metadata.submittedAt).toBeDefined();
        done();
    });

    it('should create a valid event for minimum data', function(done) {
        let testPipeline = 'Website';
        let testSource = 'Vivid';
        let testEventName = 'TestEvent';

        let metadata = metadataFunctions.buildMetadata(testPipeline, testSource, testEventName);

        expect(metadata).toBeDefined();
        expect(metadata.pipeline).toEqual(testPipeline);
        expect(metadata.source).toEqual(testSource);
        expect(metadata.eventName).toEqual(testEventName);
        expect(metadata.version).toBeDefined();
        expect(metadata.externalId).toBeDefined();
        expect(metadata.brand).toBeDefined();
        expect(metadata.locale).toBeDefined();
        expect(metadata.country).toBeDefined();
        expect(metadata.service).toBeDefined();
        expect(metadata.module).toBeDefined();
        expect(metadata.pd).toBeDefined();
        expect(metadata.sc).toBeDefined();
        expect(metadata.ingestionType).toBeDefined();
        expect(metadata.discoverable).toBeDefined();
        expect(metadata.tags).toBeDefined();
        expect(metadata.synthetic).toBeDefined();
        expect(metadata.occurredAt).toBeDefined();
        expect(metadata.submittedAt).toBeDefined();
        done();
    });

    it('should throw an error if required fields are missing', function(done) {
        let testPipeline = 'Website';
        let testSource = 'Vivid';

        let metadata = {};
        try {
            metadata = metadataFunctions.buildMetadata(testPipeline, testSource);
        }
        catch(ex) {
            expect(ex).toBeDefined();
            expect(ex.message).toEqual('Required metadata fields missing');
        }
        done();
    });

    it('should validate the supplied pipeline', function(done) {
        let testPipeline = 'Another';
        let testSource = 'Vivid';
        let testEventName = 'TestEvent';

        let metadata = {};
        try {
            metadata = metadataFunctions.buildMetadata(testPipeline, testSource, testEventName);
        }
        catch(ex) {
            expect(ex).toBeDefined();
            expect(ex.message.indexOf('Pipeline must be one of')).not.toBeLessThan(0);
        }
        done();
    });

    it('should validate the supplied source', function(done) {
        let testPipeline = 'Website';
        let testSource = 'Another';
        let testEventName = 'TestEvent';

        let metadata = {};
        try {
            metadata = metadataFunctions.buildMetadata(testPipeline, testSource, testEventName);
        }
        catch(ex) {
            expect(ex).toBeDefined();
            expect(ex.message.indexOf('Source must be one of')).not.toBeLessThan(0);
        }
        done();
    });

    it('should validate the supplied locale', function(done) {
        let testPipeline = 'Website';
        let testSource = 'Vivid';
        let testEventName = 'TestEvent';
        let testLocale = 'Another';

        let metadata = {};
        try {
            metadata = metadataFunctions.buildMetadata(testPipeline, testSource, testEventName, null, null, null, testLocale);
        }
        catch(ex) {
            expect(ex).toBeDefined();
            expect(ex.message.indexOf('Locale must be one of')).not.toBeLessThan(0);
        }
        done();
    });

    it('should validate the supplied security class', function(done) {
        let testPipeline = 'Website';
        let testSource = 'Vivid';
        let testEventName = 'TestEvent';
        let testSC = 'Another';

        let metadata = {};
        try {
            metadata = metadataFunctions.buildMetadata(testPipeline, testSource, testEventName, null, null, null, null, null, null, null, null, testSC);
        }
        catch(ex) {
            expect(ex).toBeDefined();
            expect(ex.message.indexOf('Security Classification must be one of')).not.toBeLessThan(0);
        }
        done();
    });

    it('should validate the supplied ingestion type', function(done) {
        let testPipeline = 'Website';
        let testSource = 'Vivid';
        let testEventName = 'TestEvent';
        let testIngestion = 'Another';

        let metadata = {};
        try {
            metadata = metadataFunctions.buildMetadata(testPipeline, testSource, testEventName, null, null, null, null, null, null, null, null, null, testIngestion);
        }
        catch(ex) {
            expect(ex).toBeDefined();
            expect(ex.message.indexOf('Ingestion Type must be one of')).not.toBeLessThan(0);
        }
        done();
    });
});

// buildClickMetadata
describe('Create Clickstream metadata object', function() {
    it('should provide a clickstream metadata object for given input values', function(done) {
        let testEventName = 'TestEvent';
        let testCorrelationId = uuid.v4();
        let testBrand = 'TestBrand';
        let testLocale = 'en_BORK';
        let testCountry = 'RSA';
        let testService = 'TestService';
        let testModule = 'TestModule';
        let testSynthetic = false;

        let metadata = metadataFunctions.buildClickMetadata(testEventName, testCorrelationId, testBrand, testService, testModule, testCountry, testLocale, testSynthetic);
        
        expect(metadata).toBeDefined();
        expect(metadata.pipeline).toEqual('Website');
        expect(metadata.source).toEqual('Vivid');
        expect(metadata.eventName).toEqual(testEventName);
        expect(metadata.version).toEqual(1);
        expect(metadata.externalId).toBeDefined();
        expect(metadata.brand).toEqual(testBrand);
        expect(metadata.locale).toEqual(testLocale);
        expect(metadata.country).toEqual(testCountry);
        expect(metadata.service).toEqual(testService);
        expect(metadata.module).toEqual(testModule);
        expect(metadata.pd).toEqual(false);
        expect(metadata.sc).toEqual('public');
        expect(metadata.ingestionType).toEqual('single');
        expect(metadata.discoverable).toEqual(true);
        expect(metadata.synthetic).toEqual(false);
        expect(metadata.occurredAt).toBeDefined();
        expect(metadata.submittedAt).toBeDefined();
        done();
    });

    it('should create a valid event for minimum data', function(done) {
        let testEventName = 'TestEvent';

        let metadata = metadataFunctions.buildClickMetadata(testEventName);

        expect(metadata).toBeDefined();
        expect(metadata.pipeline).toEqual('Website');
        expect(metadata.source).toEqual('Vivid');
        expect(metadata.eventName).toEqual(testEventName);
        expect(metadata.version).toEqual(1);
        expect(metadata.externalId).toBeDefined();
        expect(metadata.brand).toBeDefined();
        expect(metadata.locale).toBeDefined();
        expect(metadata.country).toBeDefined();
        expect(metadata.service).toBeDefined();
        expect(metadata.module).toBeDefined();
        expect(metadata.pd).toEqual(false);
        expect(metadata.sc).toEqual('public');
        expect(metadata.ingestionType).toEqual('single');
        expect(metadata.discoverable).toEqual(true);
        expect(metadata.synthetic).toEqual(false);
        expect(metadata.occurredAt).toBeDefined();
        expect(metadata.submittedAt).toBeDefined();
        done();
    });

    it('should throw an error if required fields are missing', function(done) {

        let metadata = {};
        try {
            metadata = metadataFunctions.buildClickMetadata();
        }
        catch(ex) {
            expect(ex).toBeDefined();
            expect(ex.message).toEqual('eventName must be supplied');
        }
        done();
    });

    it('should validate the supplied locale', function(done) {
        let testEventName = 'TestEvent';
        let testLocale = 'Another';

        let metadata = {};
        try {
            metadata = metadataFunctions.buildClickMetadata(testEventName, null, null, null, null, null, testLocale);
        }
        catch(ex) {
            expect(ex).toBeDefined();
            expect(ex.message.indexOf('Locale must be one of')).not.toBeLessThan(0);
        }
        done();
    });
});