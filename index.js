/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * Main tachyon utility function options
 *
 */

'use strict'

var metadataFunctions = require('./metadata/index'),
    telemetryFunctions = require('./telemetry/index'),
    authFunctions = require('./authentication/index'),
    msgFunctions = require('./messaging/index')
    ;

/**
 * Utility functions
 */
exports.metadata = metadataFunctions;
exports.telemetry = telemetryFunctions;
exports.authentication = authFunctions;
exports.messaging = msgFunctions;