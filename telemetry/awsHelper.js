/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * AWS telemetry utility functions
 *
 */

 'use strict'

var {
        KinesisClient,
        PutRecordCommand
    } = require('@aws-sdk/client-kinesis'),
    { 
        DynamoDBClient, 
        GetItemCommand,
        ScanCommand
    } = require('@aws-sdk/client-dynamodb'),
    {
        marshall,
        unmarshall
    } = require('@aws-sdk/util-dynamodb'),
    {
        STSClient,
        AssumeRoleCommand
    } = require('@aws-sdk/client-sts'),
    uuid = require('uuid'),
    constants = require('./constants')
    ;

let AWS_REGION = process.env.AWS_REGION || constants.DEFAULT_AWS_REGION;
let AWS_RETRIES = constants.DEFAULT_AWS_RETRIES;
if (!isNaN(process.env.AWS_RETRIES)) {
    AWS_RETRIES = parseInt(process.env.AWS_RETRIES);
}

let TELEMETRY_DELIVERY_ROLE = process.env.TELEMETRY_DELIVERY_ROLE || constants.DEFAULT_TELEMETRY_DELIVERY_ROLE;
let TELEMETRY_CONFIG_ROLE = process.env.TELEMETRY_CONFIG_ROLE || constants.DEFAULT_TELEMETRY_CONFIG_ROLE;
let CONFIG_DURATION = constants.DEFAULT_CONFIG_DURATION;
if (!isNaN(process.env.CONFIG_DURATION)) {
    CONFIG_DURATION = parseInt(process.env.CONFIG_DURATION);
}
CONFIG_DURATION = CONFIG_DURATION * 60;
let DATA_RELOAD = constants.DEFAULT_DATA_RELOAD;
if (!isNaN(process.env.DATA_RELOAD)) {
    DATA_RELOAD = parseInt(process.env.DATA_RELOAD);
}
DATA_RELOAD = DATA_RELOAD * 1000 * 60;
let DATA_DURATION = constants.DEFAULT_DATA_DURATION;
if (!isNaN(process.env.DATA_DURATION)) {
    DATA_DURATION = parseInt(process.env.DATA_DURATION);
}
DATA_DURATION = DATA_DURATION * 60;

var kinesis;
var dataCredsInterval;

/**
 * @function createKinesisConnection
 * Establish connection to kinesis for telemetry data delivery
**/
exports.createKinesisConnection = function createKinesisConnection() {
    if (TELEMETRY_DELIVERY_ROLE && TELEMETRY_DELIVERY_ROLE.length > 0) {
        reloadDataCredentials();
    }
    else {
        kinesis = new KinesisClient({
            region: AWS_REGION,
            maxAttempts: AWS_RETRIES
        });
    }
}

/**
 * @function reloadDataCredentials
 * Reloads Kinesis Credentials based on DATA_RELOAD
**/
async function reloadDataCredentials() {
    if (dataCredsInterval) {
        clearInterval(dataCredsInterval);
    }
    var errRetry = 1000 * 60 * 2;
    let stsClient = new STSClient();
    let stsData;
    let params = {
        RoleSessionName: `Telemetry-Session-${(new Date()).getTime()}`,
        RoleArn: TELEMETRY_DELIVERY_ROLE,
        DurationSeconds: DATA_DURATION
    };

    try {
        stsData = await stsClient.send(new AssumeRoleCommand(params));
    }
    catch(ex) {
        console.error('Unable to asssume data delivery role ' + TELEMETRY_DELIVERY_ROLE);
        console.error(ex);
        dataCredsInterval = setInterval(reloadDataCredentials, errRetry);
        return;
    }

    kinesis = new KinesisClient({
        region: AWS_REGION,
        credentials: {
            accessKeyId: stsData.Credentials.AccessKeyId,
            secretAccessKey: stsData.Credentials.SecretAccessKey,
            sessionToken: stsData.Credentials.SessionToken
        },
        maxAttempts: AWS_RETRIES
    });

    dataCredsInterval = setInterval(reloadDataCredentials, DATA_RELOAD);
}

/**
 * @function getDynamo
 * Get an item from dynamo for the given partition keys
 * @param {string} tableName - The dynamoDB table 
 * @param {object} tableKey - Dynamo item key
**/
exports.getDynamo = async function getDynamo(tableName, tableKey) {
    let dynamoDBClient = await createDynamoConnection();
    let params = {
        TableName: tableName,
        Key: marshall(tableKey)
    };

    let data = await dynamoDBClient.send(new GetItemCommand(params));
    let result = data.Item !== undefined ? unmarshall(data.Item) : undefined;

    return result;
}

/**
 * @function scanDynamo
 * Scan data in dynamo table
 * @param {string} tableName - The dynamoDB table
**/
exports.scanDynamo = async function scanDynamo(tableName) {
    let results = [];
    let dynamoDBClient = await createDynamoConnection();
    let params = {
        TableName: tableName
    };

    let complete = false;
    while (!complete) {
        try {
            let data = await dynamoDBClient.send(new ScanCommand(params));
            for (var i = 0; i < data.Items.length; i++) {
                results.push(unmarshall(data.Items[i]));
            }
            if (data.LastEvaluatedKey !== undefined) {
                params.ExclusiveStartKey = data.LastEvaluatedKey;
            }
            else {
                complete = true;
            }
        }
        catch(ex) {
            console.error(ex);
            complete = true;
        }
    }

    return results;
}

/**
 * @function putKinesis
 * Function to put data into Kinesis stream
 * @param {string} streamName - The kinesis stream name
 * @param {object} kinesisData - The object to add to the kinesis stream
**/
exports.putKinesis = async function putKinesis(streamName, kinesisData) {
    if (!kinesis) {
        return;
    }

    let params = {
        Data: Buffer.from(JSON.stringify(kinesisData) + '\n'),
        PartitionKey: uuid.v4(),
        StreamName: streamName
    };

    try {
        await kinesis.send(new PutRecordCommand(params));
    }
    catch(ex) {
        console.log(ex);
    }
}

/**
 * @function createDynamoConnection
 * Create connection to dynamoDB for telemetry account
**/
async function createDynamoConnection() {
    let dynamoDBClient;
    if (TELEMETRY_CONFIG_ROLE && TELEMETRY_CONFIG_ROLE.length > 0) {
        let stsClient = new STSClient();
        let params = {
            RoleSessionName: `Dynamo-Session-${(new Date()).getTime()}`,
            RoleArn: TELEMETRY_CONFIG_ROLE,
            DurationSeconds: CONFIG_DURATION
        };
    
        let stsData = await stsClient.send(new AssumeRoleCommand(params));
        dynamoDBClient = new DynamoDBClient({
            region: AWS_REGION,
            credentials: {
                accessKeyId: stsData.Credentials.AccessKeyId,
                secretAccessKey: stsData.Credentials.SecretAccessKey,
                sessionToken: stsData.Credentials.SessionToken
            },
            maxAttempts: AWS_RETRIES
        });
    }
    else {
        dynamoDBClient = new DynamoDBClient({
            region: AWS_REGION,
            maxAttempts: AWS_RETRIES
        });
    }
    return dynamoDBClient;
}