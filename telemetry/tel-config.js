/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * Telemetry config utility
 *
**/

'use strict'

var aws = require('./awsHelper'),
    constants = require('./constants')
    ;

let TELEMETRY_TABLE = process.env.TELEMETRY_TABLE || constants.DEFAULT_TELEMETRY_TABLE;

/**
 * @function getTelemetryFields
 * Function to get the metadata fields required for telemetry use in the current environment
**/
exports.getTelemetryFields = async function getTelemetryFields() {
    let results;
    try {
        results = await aws.scanDynamo(TELEMETRY_TABLE);
        console.log('Successfully obtained local telemetry config');
    }
    catch(ex) {
        console.warn('Unable to get telemetry config from local environment table');
        console.warn(ex);
        results = [
            {
                clientId: 'default',
                telFields: constants.DEFAULT_TELEMETRY_FIELDS
            }
        ];
    }
    
    return results;
}