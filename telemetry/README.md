# Tachyon Telemetry

The Tachyon Telemetry package is a common utility for all applications to provide telemetry related to their functionality. System diagnostic data is provided and for apps dealing with event data, there are options to supply details on the number and type of events being handled.

The telemetry package reference allows all modules within an application to interact with a common instance because javascript uses the first in-memory version for all further references. As such, once the telemetry is started, any modules can add event/pii/error data to the system telemetry.

---

## Start Telemetry

The Tachyon Telemetry package will only start sending telemetry data to the system kinesis stream after it has been successfully started. This process involves working out the metadata fields that are used for telemetry in the environment it is running within, obtaining task-specific container data to differentiate tasks within common services, and scheduling the capture of system diagnostic information.

The name and version of the app should be supplied when starting telemetry, and it is suggested that this be taken from the app's package.json file.

```js
var tacUtils = require('tac-1d-utils'),
    appPackage = require('./package.json')
    ;

tacUtils.telemetry.startTelemetry(appPackage.name, appPackage.version);
```

---

## Add Telemetry Events

When event data is processed within applications it can be added to the telemetry data by passing in the event metadata object. The environment's telemetry configuration determines the metadata fields that are then included into the event telemetry data.

Functions are provided to allow the addition of successful event processing, successful personal data event processing, and event errors:

### Add Successful Event

```js
tacUtils.telemetry.addTelEvent(eventData.metadata);
```

### Add Successful Personal Data (PII) Event

```js
tacUtils.telemetry.addTelPII(eventData.metadata);
```

### Add Event Error

```js
tacUtils.telemetry.addTelError(eventData.metadata);
```

---

## Telemetry Environment Variables

In order for the telemetry package to function correctly, environment variables should be set to provide settings applicable to the application running an instance of telemetry. Those highlighted in bold should be altered per application.

Environment Variable | Description | Default
--- | --- | ---
TELEMETRY_INTERVAL | The interval (in milliseconds) at which new telemetry data is delivered to the system kinesis stream | 1000
TELEMETRY_ENABLED | Whether telemetry is enabled for an application | true
**TELEMETRY_STREAM_NAME** | The name of the system kinesis stream | tac-1d-tdata01-system
**TELEMETRY_CONTROL_ENDPOINT** | The Tachyon Control API endpoint | 
**TELEMETRY_TABLE** | Local DynamoDB table to store telemetry config | tac-1d-tel-config
**TELEMETRY_TABLE_KEY** | The partition key for the local telemetry config table | tac-1d-tel-partKey
**AWS_REGION** | The AWS region to use | eu-west-1
AWS_SOCKET_TIMEOUT | Socket timeout (in milliseconds) on AWS resources | 60000
AWS_RETRIES | Number of retries on AWS resources | 5

---

[Return to root documentation](./../README.md#tachyon-utilities)