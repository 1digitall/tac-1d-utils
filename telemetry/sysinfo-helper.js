/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * System performance utility
 *
**/

'use strict'

var si = require('systeminformation')
    ;

let telPerf = {
    diskIO: [],
    fsStat: [],
    netStat: [],
    cpuStat: [],
    memStat: [],
    netPrev: {
        rBytes: -1,
        tBytes: -1,
        rError: -1,
        tError: -1,
        rDropped: -1,
        tDropped: -1
    }
};

/**
 * @function capturePerformance
 * Function to get current hardware performance metrics
**/
exports.capturePerformance = function capturePerformance() {
    si.currentLoad()
    .then(data => {
        telPerf.cpuStat.push(data);
        if (telPerf.cpuStat.length > 1) {
            telPerf.cpuStat.shift();
        }
    })
    .catch(error => console.error(error));

    si.mem()
    .then(data => {
        telPerf.memStat.push(data);
        if (telPerf.memStat.length > 1) {
            telPerf.memStat.shift();
        }
    })
    .catch(error => console.error(error));

    si.disksIO()
    .then(data => {
        telPerf.diskIO.push(data);
        if (telPerf.diskIO.length > 1) {
            telPerf.diskIO.shift();
        }
    })
    .catch(error => console.error(error));

    si.fsStats()
    .then(data => {
        telPerf.fsStat.push(data);
        if (telPerf.fsStat.length > 1) {
            telPerf.fsStat.shift();
        }
    })
    .catch(error => console.error(error));

    si.networkStats()
    .then(data => {
        let combinedData = {};
        combinedData.rx_sec = data.reduce((total, next) => total + next.rx_sec, 0);
        combinedData.rx_dropped = data.reduce((total, next) => total + next.rx_dropped, 0);
        combinedData.rx_errors = data.reduce((total, next) => total + next.rx_errors, 0);
        combinedData.rx_bytes = data.reduce((total, next) => total + next.rx_bytes, 0);
        combinedData.tx_sec = data.reduce((total, next) => total + next.tx_sec, 0);
        combinedData.tx_dropped = data.reduce((total, next) => total + next.tx_dropped, 0);
        combinedData.tx_errors = data.reduce((total, next) => total + next.tx_errors, 0);
        combinedData.tx_bytes = data.reduce((total, next) => total + next.tx_bytes, 0);
        telPerf.netStat.push(combinedData);
        if (telPerf.netStat.length > 1) {
            telPerf.netStat.shift();
        }
    })
    .catch(error => console.error(error));
}

/**
 * @function buildPerformance
 * Function to build performance object for use in telemetry data
**/
exports.buildPerformance = function buildPerformance() {
    let perf = {
        cpu: {
            load: -1,
            idle: -1
        },
        mem: {
            used: -1,
            free: -1,
            total: 0
        },
        network: {
            receive: {
                data: -1,
                errors: 0,
                dropped: 0
            },
            send: {
                data: -1,
                errors: 0,
                dropped: 0        
            }
        },
        disk: {
            io: {
                read: -1,
                write: -1,
                total: 0
            },
            transfer: {
                read: -1,
                write: -1,
                total: 0
            }
        }
    };

    if (telPerf.cpuStat.length > 0) {
        try {
            perf.cpu.load = telPerf.cpuStat[telPerf.cpuStat.length - 1].currentLoad;
            perf.cpu.idle = telPerf.cpuStat[telPerf.cpuStat.length - 1].currentLoadIdle;
        }
        catch(ex) {}
    }
    if (telPerf.memStat.length > 0) {
        try {
            perf.mem.total = telPerf.memStat[telPerf.memStat.length - 1].total;
            perf.mem.used = telPerf.memStat[telPerf.memStat.length - 1].used;
            perf.mem.free = telPerf.memStat[telPerf.memStat.length - 1].free;
        }
        catch(ex) {}
    }
    if (telPerf.diskIO.length > 0 && telPerf.diskIO[telPerf.diskIO.length - 1].tIO_sec > 0) {
        try {
            perf.disk.io.read = telPerf.diskIO[telPerf.diskIO.length - 1].rIO_sec;
            perf.disk.io.write = telPerf.diskIO[telPerf.diskIO.length - 1].wIO_sec;
            perf.disk.io.total = telPerf.diskIO[telPerf.diskIO.length - 1].tIO_sec;
        }
        catch(ex) {}
    }
    if (telPerf.fsStat.length > 0 && telPerf.fsStat[telPerf.fsStat.length - 1].tx_sec > 0) {
        try {
            perf.disk.transfer.read = telPerf.fsStat[telPerf.fsStat.length - 1].rx_sec;
            perf.disk.transfer.write = telPerf.fsStat[telPerf.fsStat.length - 1].wx_sec;
            perf.disk.transfer.total = telPerf.fsStat[telPerf.fsStat.length - 1].tx_sec;
        }
        catch(ex) {}
    }
    if (telPerf.netStat.length > 0) {
        try {
            perf.network.receive.data = telPerf.netStat[telPerf.netStat.length - 1].rx_sec;
            perf.network.send.data = telPerf.netStat[telPerf.netStat.length - 1].tx_sec;

            if (telPerf.netPrev.rError < 0 && telPerf.netStat[telPerf.netStat.length - 1].rx_errors >= 0) {
                telPerf.netPrev.rError = telPerf.netStat[telPerf.netStat.length - 1].rx_errors;
            }
            if (telPerf.netPrev.tError < 0 && telPerf.netStat[telPerf.netStat.length - 1].tx_errors >= 0) {
                telPerf.netPrev.tError = telPerf.netStat[telPerf.netStat.length - 1].tx_errors;
            }
            if (telPerf.netPrev.rDropped < 0 && telPerf.netStat[telPerf.netStat.length - 1].rx_dropped >= 0) {
                telPerf.netPrev.rDropped = telPerf.netStat[telPerf.netStat.length - 1].rx_dropped;
            }
            if (telPerf.netPrev.tDropped < 0 && telPerf.netStat[telPerf.netStat.length - 1].tx_dropped >= 0) {
                telPerf.netPrev.tDropped = telPerf.netStat[telPerf.netStat.length - 1].tx_dropped;
            }
            if (telPerf.netPrev.rBytes < 0 && telPerf.netStat[telPerf.netStat.length - 1].rx_bytes >= 0) {
                telPerf.netPrev.rBytes = telPerf.netStat[telPerf.netStat.length - 1].rx_bytes;
            }
            if (telPerf.netPrev.tBytes < 0 && telPerf.netStat[telPerf.netStat.length - 1].tx_bytes >= 0) {
                telPerf.netPrev.tBytes = telPerf.netStat[telPerf.netStat.length - 1].tx_bytes;
            }

            perf.network.receive.errors = telPerf.netStat[telPerf.netStat.length - 1].rx_errors - telPerf.netPrev.rError;
            telPerf.netPrev.rError = telPerf.netStat[telPerf.netStat.length - 1].rx_errors;
            perf.network.receive.dropped = telPerf.netStat[telPerf.netStat.length - 1].rx_dropped - telPerf.netPrev.rDropped;
            telPerf.netPrev.rDropped = telPerf.netStat[telPerf.netStat.length - 1].rx_dropped;
            //perf.network.receive.bytes = telPerf.netStat[telPerf.netStat.length - 1].rx_bytes - telPerf.netPrev.rBytes;
            telPerf.netPrev.rBytes = telPerf.netStat[telPerf.netStat.length - 1].rx_bytes;

            perf.network.send.errors = telPerf.netStat[telPerf.netStat.length - 1].tx_errors - telPerf.netPrev.tError;
            telPerf.netPrev.tError = telPerf.netStat[telPerf.netStat.length - 1].tx_errors;
            perf.network.send.dropped = telPerf.netStat[telPerf.netStat.length - 1].tx_dropped - telPerf.netPrev.tDropped;
            telPerf.netPrev.tDropped = telPerf.netStat[telPerf.netStat.length - 1].tx_dropped;
            //perf.network.send.bytes = telPerf.netStat[telPerf.netStat.length - 1].tx_bytes - telPerf.netPrev.tBytes;
            telPerf.netPrev.tBytes = telPerf.netStat[telPerf.netStat.length - 1].tx_bytes;
        }
        catch(ex) {}
    }

    return perf;
}