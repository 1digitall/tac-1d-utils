/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * Tachyon telemetry functionality
 *
**/

'use strict'

var uuid = require('uuid'),
    telConfig = require('./tel-config'),
    taskFunctions = require('./task-helper'),
    sysinfoFunctions = require('./sysinfo-helper'),
    aws = require('./awsHelper'),
    constants = require('./constants')
    ;

let TELEMETRY_INTERVAL = process.env.TELEMETRY_INTERVAL || constants.DEFAULT_TELEMETRY_INTERVAL;
let TELEMETRY_ENABLED = constants.DEFAULT_TELEMETRY_ENABLED;
if (process.env.TELEMETRY_ENABLED) {
    TELEMETRY_ENABLED = (process.env.TELEMETRY_ENABLED === 'true');
} 
let TELEMETRY_STREAM_NAME = process.env.TELEMETRY_STREAM_NAME || constants.DEFAULT_TELEMETRY_STREAM_NAME;
let TELEMETRY_FIELDS = [];
let TELEMETRY_RUNNING = false;

let telEvents = {
    events: [],
    pii: [],
    errors: []
};

let telTime;
let telApp = {};

/**
 * @function startTelemetry
 * Function to start telemetry collection and submission
**/
exports.startTelemetry = async function startTelemetry(packageName, packageVersion) {
    if (TELEMETRY_RUNNING) {
        return;
    }

    if (!packageVersion) {
        packageVersion = '0.0.0';
    }

    if (TELEMETRY_ENABLED && packageName && packageName.length > 0) {
        console.log('Starting application telemetry for ' + packageName + ' version ' + packageVersion);
        let telFields = await telConfig.getTelemetryFields();
        if (telFields.length > 0) {
            TELEMETRY_FIELDS = telFields;
            await aws.createKinesisConnection();
            let taskData = await taskFunctions.getContainerData(packageName, packageVersion);
            telApp = taskData;
            TELEMETRY_RUNNING = true;
            console.log('Application telemetry is now running');

            setInterval(function() {
                sysinfoFunctions.capturePerformance();
            }, 1000);
    
            setInterval(function() {
                let telData = buildTelemetry();
                aws.putKinesis(TELEMETRY_STREAM_NAME, telData);
            }, TELEMETRY_INTERVAL);
        }
        else {
            console.error('Telemetry configuration has no fields set for tracking');
            TELEMETRY_ENABLED = false;
            telEvents = {
                events: [],
                pii: [],
                errors: []
            };
        }
    }
    else {
        console.log('Application telemetry is currently disabled');
        TELEMETRY_ENABLED = false;
        telEvents = {
            events: [],
            pii: [],
            errors: []
        };
    }
}

/**
 * @function addTelEvent
 * Function to add successful event to telemetry
 * @param {object} metadata - The event metadata object
**/
exports.addTelEvent = function addTelEvent(metadata) {
    if (TELEMETRY_ENABLED && TELEMETRY_FIELDS.length > 0) {
        let telEvent = {};
        let clientTel = TELEMETRY_FIELDS.find(e => e.clientId === 'default');
        if (metadata.cid) {
            clientTel = TELEMETRY_FIELDS.find(e => e.clientId === metadata.cid) || clientTel;
        }
        if (clientTel !== undefined && clientTel.telFields.length > 0) {
            telEvent.clientId = clientTel.clientId;
            for (var i = 0; i < clientTel.telFields.length; i++) {
                if (metadata[Object.keys(metadata).find(e => e.toLowerCase() === clientTel.telFields[i].toLowerCase())]) {
                    telEvent[clientTel.telFields[i]] = metadata[Object.keys(metadata).find(e => e.toLowerCase() === clientTel.telFields[i].toLowerCase())].toString().toLowerCase();
                }
                else {
                    telEvent[clientTel.telFields[i]] = '';
                }
            }
            telEvents.events.push(telEvent);
        }
    }
}

/**
 * @function addTelPII
 * Function to add PII event to telemetry
 * @param {object} metadata - The pii event metadata object
**/
exports.addTelPII = function addTelPII(metadata) {
    if (TELEMETRY_ENABLED && TELEMETRY_FIELDS.length > 0) {
        let telPII = {};
        let clientTel = TELEMETRY_FIELDS.find(e => e.clientId === 'default');
        if (metadata.cid) {
            clientTel = TELEMETRY_FIELDS.find(e => e.clientId === metadata.cid) || clientTel;
        }
        if (clientTel !== undefined && clientTel.telFields.length > 0) {
            telPII.clientId = clientTel.clientId;
            for (var i = 0; i < clientTel.telFields.length; i++) {
                if (metadata[Object.keys(metadata).find(e => e.toLowerCase() === clientTel.telFields[i].toLowerCase())]) {
                    telPII[clientTel.telFields[i]] = metadata[Object.keys(metadata).find(e => e.toLowerCase() === clientTel.telFields[i].toLowerCase())].toString().toLowerCase();
                }
                else {
                    telPII[clientTel.telFields[i]] = '';
                }
            }
            telEvents.pii.push(telPII);
        }
    }
}

/**
 * @function addTelError
 * Function to add event error to telemetry
 * @param {object} metadata - The error event metadata object
**/
exports.addTelError = function addTelError(metadata) {
    if (TELEMETRY_ENABLED && TELEMETRY_FIELDS.length > 0) {
        let telError = {};
        let clientTel = TELEMETRY_FIELDS.find(e => e.clientId === 'default');
        if (metadata.cid) {
            clientTel = TELEMETRY_FIELDS.find(e => e.clientId === metadata.cid) || clientTel;
        }
        if (clientTel !== undefined && clientTel.telFields.length > 0) {
            telError.clientId = clientTel.clientId;
            for (var i = 0; i < clientTel.telFields.length; i++) {
                if (metadata[Object.keys(metadata).find(e => e.toLowerCase() === clientTel.telFields[i].toLowerCase())]) {
                    telError[clientTel.telFields[i]] = metadata[Object.keys(metadata).find(e => e.toLowerCase() === clientTel.telFields[i].toLowerCase())].toString().toLowerCase();
                }
                else {
                    telError[clientTel.telFields[i]] = '';
                }
            }
            telEvents.errors.push(telError);
        }
    }
}

/**
 * @function condenseTelemetryArray
 * Function to condense telemetry array data that has arrived in the timeframe
 * @param {array} eventData - The event data to condense
**/
function condenseTelemetryArray(eventData) {
    let events = [];
    let telEventsCopy = JSON.parse(JSON.stringify(eventData));

    for (var i = 0; i < telEventsCopy.length; i++) {
        let clientTel = TELEMETRY_FIELDS.find(e => e.clientId === telEventsCopy[i].clientId);
        let itemIndex = events.map(function(e) {
            var result = true;
            for (var j = 0; j < clientTel.telFields.length; j++) {
                if (e[clientTel.telFields[j]] !== telEventsCopy[i][clientTel.telFields[j]]) {
                    result = false;
                    break;
                }
            }
            return result;
        }).indexOf(true);
        if (itemIndex >= 0) {
            events[itemIndex].total++;
        }
        else {
            let item = JSON.parse(JSON.stringify(telEventsCopy[i]));
            item.total = 1;
            events.push(item);
        }

        eventData.shift();
    }

    return events;
}

/**
 * @function buildTelemetry
 * Function to build telemetry data object
**/
function buildTelemetry() {
    let tel = {
        metadata: {
            pipeline: 'telemetry',
            eventName: 'telemetry',
            eventId: uuid.v4(),
            submittedAt: (new Date()).toISOString()
        },
        payload: {
            app: telApp,
            perf: sysinfoFunctions.buildPerformance(),
            startTime: telTime,
            endTime: (new Date()).toISOString()
        }
    };

    telTime = tel.payload.endTime;

    tel.payload.events = condenseTelemetryArray(telEvents.events);
    tel.payload.pii = condenseTelemetryArray(telEvents.pii);
    tel.payload.errors = condenseTelemetryArray(telEvents.errors);

    tel.payload.totals = [];

    for (var i = 0; i < tel.payload.events.length; i++) {
        let clientIndex = tel.payload.totals.findIndex(e => e.clientId === tel.payload.events[i].clientId);
        if (clientIndex >= 0) {
            tel.payload.totals[clientIndex].events += tel.payload.events[i].total;
        }
        else {
            tel.payload.totals.push({
                clientId: tel.payload.events[i].clientId,
                events: tel.payload.events[i].total,
                pii: 0,
                errors: 0
            });
        }
    }

    for (var i = 0; i < tel.payload.pii.length; i++) {
        let clientIndex = tel.payload.totals.findIndex(e => e.clientId === tel.payload.pii[i].clientId);
        if (clientIndex >= 0) {
            tel.payload.totals[clientIndex].pii += tel.payload.pii[i].total;
        }
        else {
            tel.payload.totals.push({
                clientId: tel.payload.pii[i].clientId,
                events: 0,
                pii: tel.payload.pii[i].total,
                errors: 0
            });
        }
    }

    for (var i = 0; i < tel.payload.errors.length; i++) {
        let clientIndex = tel.payload.totals.findIndex(e => e.clientId === tel.payload.errors[i].clientId);
        if (clientIndex >= 0) {
            tel.payload.totals[clientIndex].errors += tel.payload.errors[i].total;
        }
        else {
            tel.payload.totals.push({
                clientId: tel.payload.errors[i].clientId,
                events: 0,
                pii: 0,
                errors: tel.payload.errors[i].total
            });
        }
    }

    return tel;
}

/**
 * Export local functions for unit testing
 */
exports.condenseTelemetryArray = condenseTelemetryArray;
exports.buildTelemetry = buildTelemetry;