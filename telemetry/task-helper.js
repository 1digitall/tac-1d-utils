/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * Task and application containter utility
 *
**/

'use strict'

var fs = require('fs'),
    constants = require('./constants')
    ;

let METADATA_PATH = process.env.ECS_CONTAINER_METADATA_FILE || '';
let RETRY_INTERVAL = constants.DEFAULT_TELEMETRY_CONFIG_RETRY_INTERVAL;

/**
 * @function getContainerData
 * Function to task container data for use in app information
**/
exports.getContainerData = function getContainerData(packageName, packageVersion) {
    return new Promise((resolve) => {
        var i = 0;
        let telApp = {
            name: packageName,
            version: packageVersion,
            cluster: '',
            containerName: '',
            imageName: '',
            taskID: ''
        };

        if (METADATA_PATH && METADATA_PATH.length > 0) {
            getFileData(METADATA_PATH);
        }
        else {
            console.warn('WARNING: Container file environment variable missing');
            resolve(telApp);
        }

        function getFileData(path) {
            let metadataFile = fs.readFileSync(path, 'utf8');
            let fileStatus = '';
            let fileJSON = '';
            try {
                fileJSON = JSON.parse(metadataFile);
                fileStatus = fileJSON.MetadataFileStatus;
            }
            catch(ex) {
                console.warn('Error reading metadata file... will try again');
                console.warn(ex);
                fileStatus = '';
            }

            if (fileStatus === 'READY') {
                telApp.cluster = fileJSON.Cluster || '';
                telApp.containerName = fileJSON.ContainerName || '';
                telApp.imageName = fileJSON.ImageName || '';

                let taskID = fileJSON.TaskARN || '';
                if (taskID.indexOf('/') >= 0) {
                    taskID = taskID.substr(taskID.lastIndexOf('/') + 1);
                }
                telApp.taskID = taskID;
                resolve(telApp);
            }
            else if (i < 5) {
                console.warn('Metadata file is not ready... try again');
                i++;
                setTimeout(function() {
                    getFileData(path);
                }, RETRY_INTERVAL);
            }
            else {
                console.warn('WARNING: Container data is not available for use in telemetry');
                resolve(telApp);
            }
        }
    });
}