/**
 * @author Pete Leyland <pleyland@1digit.co.uk>
 * @file
 * Telemetry constants
 *
**/

'use strict'

module.exports = Object.freeze({
    DEFAULT_TELEMETRY_ENABLED: true,
    DEFAULT_TELEMETRY_INTERVAL: 1000,
    DEFAULT_TELEMETRY_CONFIG_RETRY_INTERVAL: 30000,
    DEFAULT_TELEMETRY_TABLE: 'tac-1d-tel-config',
    DEFAULT_TELEMETRY_TABLE_KEY: 'tac-1d-tel-partKey',
    DEFAULT_TELEMETRY_STREAM_NAME: 'tac-1d-tdata01-system',
    DEFAULT_TELEMETRY_CONTROL_ENDPOINT: '',
    DEFAULT_TELEMETRY_FIELDS: [
        'pipeline',
        'source',
        'eventName'
    ],
    DEFAULT_AWS_REGION: 'eu-west-1',
    DEFAULT_AWS_RETRIES: 5,
    DEFAULT_TELEMETRY_DELIVERY_ROLE: '',
    DEFAULT_TELEMETRY_CONFIG_ROLE: '',
    DEFAULT_DATA_RELOAD: 30,
    DEFAULT_DATA_DURATION: 60,
    DEFAULT_CONFIG_DURATION: 10
});